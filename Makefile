default: help
.PHONY: mac_bootstrap debian_bootstrap desktop
OS := $(shell uname)

help:
	@echo "make mac_bootstrap - bootstrap on mac"
	@echo "make debian_bootstrap - bootstrap on debian"
	@echo "make desktop - call desktop playbook"

mac_bootstrap:
	brew update
	brew install ansible
	brew install git
	brew install python
	pip install --user ansible-lint
	pip install --user passlib
	pip install --user pycurl

debian_bootstrap:
	sudo apt install git ansible python

desktop:
ifeq ($(OS),Linux)
	cd ansible && ansible-playbook -K -i inventory desktop.yml
else
	cd ansible && ansible-playbook -i inventory desktop.yml
endif
