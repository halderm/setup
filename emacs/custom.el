;; -------------------- admin stuff
;; check os we are running on
(defvar gnulinux-p (string-match "gnu/linux" (symbol-name system-type)))
(defvar macosx-p (string-match "darwin" (symbol-name system-type)))
(defvar mswindows-p (string-match "windows" (symbol-name system-type)))

;; enable server mode for shell client
(server-start)

;; disable menu
(menu-bar-mode -1)

(setq org-html-use-infojs nil)
(setq org-publish-project-alist
      '(("orgfiles"
         :base-directory "~/orgmode"
         :base-extension "org"
         :publishing-directory "~/public_html"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 4
         :auto-preamble t
         :section-numbers t
         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-title "index"
         :html-head-include-default-style nil
         :html-head-include-scripts nil
         :html-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://www.pirilampo.org/styles/readtheorg/css/htmlize.css\"/>
                     <link rel=\"stylesheet\" type=\"text/css\" href=\"https://www.pirilampo.org/styles/readtheorg/css/readtheorg.css\"/>
                     <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js\"></script>
                     <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>
                     <script type=\"text/javascript\" src=\"https://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.min.js\"></script>
                     <script type=\"text/javascript\" src=\"https://www.pirilampo.org/styles/readtheorg/js/readtheorg.js\"></script>"
        )
        ("orgstatic"
         :base-directory "~/orgmode"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
         :publishing-directory "~/public_html/"
         :recursive t
         :publishing-function org-publish-attachment
        )
        ("org" :components ("orgfiles" "orgstatic"))
       )
)

;; add dired-async-mode
(autoload 'dired-async-mode "~/src/emacs-async/dired-async.el" nil t)
(dired-async-mode 1)

;; -------------------- mac stuff
;; use cmd as meta on mac
(if macosx-p
  (progn
    (setq mac-option-key-is-meta nil)
    (setq mac-command-key-is-meta t)
    (setq mac-command-modifier 'meta)
    (setq mac-option-modifier 'super)))

;; path and exec-path variable with macports
(if macosx-p
  (progn
    (setenv "PATH" (concat "/opt/local/sbin:/opt/local/bin:" (getenv "PATH")))
    (setq exec-path (append '("/opt/local/sbin") '("/opt/local/bin") exec-path))))

;; -------------------- plugins
;; undo tree
(add-to-list 'load-path "~/src/undo-tree")
(require 'undo-tree)
(global-undo-tree-mode)

;; deft
(add-to-list 'load-path "~/src/deft" t)
(require 'deft)
(setq deft-extension "org")
(setq deft-directory "~/orgmode")
(setq deft-text-mode 'org-mode)
(setq deft-use-filename-as-title t)
(global-set-key (kbd "C-c v") 'deft)

;; babel
(setq org-confirm-babel-evaluate nil)

;; active babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (emacs-lisp . t)
   (sh . t)
   (python . t)
   ))

;; broken libs for org
(require 'ox-md nil t)

;; map vim move keys for evil org-mode
(mapcar (lambda (state)
  (evil-declare-key state org-mode-map
    (kbd "M-l") 'org-metaright
    (kbd "M-h") 'org-metaleft
    (kbd "M-k") 'org-metaup
    (kbd "M-j") 'org-metadown
    (kbd "M-L") 'org-shiftmetaright
    (kbd "M-H") 'org-shiftmetaleft
    (kbd "M-K") 'org-shiftmetaup
    (kbd "M-J") 'org-shiftmetadown))
  '(normal insert))

;; disable flyspell mode by default
(setq prelude-flyspell nil)
(setq whitespace-line-column 200)

;; -------------------- org general setup
;; open .org and .org_archive files with org-mode
(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\)$" . org-mode))

;; useful key mappings
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

;; shortcut to custom files
(set-register ?e (cons 'file "~/setup/emacs/custom.el"))
(set-register ?o (cons 'file "~/orgmode/life.org"))
(set-register ?w (cons 'file "~/orgmode/work.org"))
(set-register ?k (cons 'file "~/orgmode/knowhow.org"))

;; define default drawers
(setq org-drawers (quote ("properties" "clocktable" "logbook" "clock")))

;; hide leading stars
(setq org-hide-leading-stars 'hidestars)

;; use helm everywhere
(setq org-completion-use-ido nil)
(setq org-outline-path-complete-in-steps nil)

;; use only 1 separator line
(setq org-cycle-separator-lines 1)

;; enforce ordered tasks
(setq org-enforce-todo-checkbox-dependencies t)
(setq org-enforce-todo-dependencies t)
(setq org-track-ordered-property-with-tag "ordered")

;; -------------------- org agenda
;; set agenda file
(setq org-agenda-files (quote ("~/orgmode")))

;; highlight current time in agenda
(add-hook 'org-agenda-mode-hook '(lambda () (hl-line-mode 1 )))

;; display priorities
(setq org-agenda-fontify-priorities
   (quote ((65 (:foreground "firebrick")) (66 (:foreground "blue")) (67 (:foreground "dark green")))))

;; mark weekend
(setq org-agenda-date-weekend (quote (:foreground "yellow" :weight bold)))

;; hide tasks already done
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)

;; hide items tagged with someday
(setq org-agenda-filter-preset '("-someday"))

;; custom agenda views
(setq org-agenda-custom-commands
   (quote
    (("z" "admin tasks" todo "admin" nil)
     ("w" "waiting tasks" todo "waiting" nil)
     ("s" "someday tasks" tags "someday"
      ((org-agenda-filter-preset
        (quote
         ("+someday")))
       (org-agenda-todo-ignore-with-date nil)))
     ("u" "urgent tasks and phone calls"
      ((tags "urgent"
             ((org-agenda-overriding-header "urgent tasks")))
       (tags "phone"
             ((org-agenda-overriding-header "phone calls"))))
      nil nil))))

;; -------------------- org todos
;; set keywords
(setq org-todo-keywords
  (quote ((sequence "todo(t)" "next(n)" "started(s)" "|" "done(d)")
          (sequence "waiting(w@/!)" "hold(h@/!)" "delegated(g@/!)" "|" "canceled(c@/!)")
          (sequence "appt(a)" "|" "proj(p)" "meeting(m)" "admin(z)"))))

;; set tags
(setq org-tag-persistent-alist
  (quote (("home" . ?h) ("work" . ?w) ("computer" . ?c) ("phone" . ?p)
         ("errands" . ?e) ("urgent" . ?u) ("someday" . ?s))))

;; change colors
(setq org-todo-keyword-faces
  (quote (("todo" :foreground "red" :weight bold)
          ("next" :foreground "firebrick" :weight bold)
          ("started" :foreground "dark red" :weight bold)
          ("done" :foreground "dark green" :weight bold)
          ("waiting" :foreground "dark orange" :weight bold)
          ("hold" :foreground "maroon" :weight bold)
          ("delegated" :foreground "forest green" :weight bold)
          ("canceled" :foreground "grey" :weight bold)
          ("appt" :foreground "sienna" :weight bold)
          ("proj" :foreground "blue" :weight bold)
          ("meeting" :foreground "dark goldenrod" :weight bold)
          ("admin" :foreground "royal blue" :weight bold))))

(setq org-priority-faces
  (quote ((?A :foreground "firebrick" :weight bold)
          (?B :foreground "blue")
          (?C :foreground "dark green"))))

;; change font size
(set-face-attribute 'default nil :height 110)

;; fast todos selection
(setq org-use-fast-todo-selection t)

;; add timestamp when done and rescheduled
(setq org-log-done (quote time))
(setq org-log-reschedule (quote time))

;; use logbook drawer for logs
(setq org-log-into-drawer t)

;; -------------------- org refile
;; refile targets
(setq org-refile-targets (quote ((org-agenda-files :maxlevel . 2) (nil :maxlevel . 5))))

;; targets start with the buffer name
(setq org-refile-use-outline-path (quote buffer-name))

(setq org-refile-allow-creating-parent-nodes t)

;; -------------------- org timer
;; save clock in drawer
(setq org-clock-into-drawer "CLOCK")

;; resume clocking tasks when emacs is restarted
(org-clock-persistence-insinuate)

;; sesume clocking task on clock-in if the clock is open
(setq org-clock-in-resume t)

;; don't clock out when moving task to a done state
(setq org-clock-out-when-done nil)

;; persist clock
(setq org-clock-persist t)

;; disable auto clock resolution
(setq org-clock-auto-clock-resolution nil)

;; persist clock
(setq org-publish-use-timestamps-flag nil)

;; -------------------- org capture
;; setup org-capture
(setq org-default-notes-file (concat org-directory "~/orgmode/life.org"))
(define-key global-map "\C-cc" 'org-capture)
(setq org-capture-templates
 '(("t" "task" entry (file+headline "~/orgmode/life.org" "inbox")
        "* todo %?")
   ("c" "task with context" entry (file+headline "~/orgmode/life.org" "inbox")
        "* todo %?\n  %i\n  %a")
   ("i" "immediate admin task" entry (file+headline "~/orgmode/life.org" "inbox")
        "* admin %?" :clock-in t :clock-keep t)
   ("z" "admin task" entry (file+headline "~/orgmode/life.org" "inbox")
        "* admin %?\n  %i\n  %a" :clock-in t :clock-resume t)
   ("j" "journal" entry (file+datetree "~/orgmode/life.org")
        "* %?\nentered on %U\n  %i\n  %a")))

;; -------------------- org column view
;; setup default format
(setq org-columns-default-format "%7todo %50item %10effort{:} %10clocksum")

;; setup default estimates
(setq org-global-properties (quote (("effort_all" . "0:30 1:00 2:00 4:00 8:00"))))

;; setup default browser
(if macosx-p
  (progn
    (setq browse-url-browser-function 'browse-url-default-macosx-browser))
  (progn
    (setq browse-url-browser-function 'browse-url-chromium)))

;; custom variables
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (csv-mode geiser company-anaconda anaconda-mode json-mode js2-mode rainbow-mode elisp-slime-nav rainbow-delimiters evil-numbers evil-visualstar evil-surround evil key-chord company helm-ag helm-descbinds helm-projectile helm smex ido-completing-read+ flx-ido exec-path-from-shell zop-to-char zenburn-theme which-key volatile-highlights undo-tree super-save smartrep smartparens operate-on-number move-text magit projectile imenu-anywhere hl-todo guru-mode gitignore-mode gitconfig-mode git-timemachine gist flycheck expand-region epl editorconfig easy-kill diminish diff-hl discover-my-major crux browse-kill-ring beacon anzu ace-window))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
