if [ $(uname) = 'Darwin' ]; then
    alias l='ls -G -l -h' # long, human, color
    alias ls='ls -G'
    alias la='ls -G -l -A' # long, almost all, color
    alias l.='ls -l -d .[^.]*'
else
    # activate autojump
    [ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh

    alias l='ls --color -l -h' # long, human, color
    alias ls='ls --color'
    alias la='ls --color -l -A' # long, almost all, color
    alias l.='ls -l -d .*'
fi

# set default editor
export EDITOR='vim'
export VISUAL='vim -f'

# switch of annoying beep
setopt nobeep

# take extension and completion to its limit
setopt extendedglob

# do not use first match automatically
unsetopt menucomplete

# list choices of completion
setopt autolist

# no auto correction
unsetopt correct_all

# exit shell with background jobs
setopt nocheckjobs
setopt nohup

# directory ops
alias md='mkdir -p'
alias s='cd ..'
alias ss='cd ../..'
alias e='exit'

# vim
alias gv='gvim --remote-silent'
alias gw='gvim -f'
alias gvs='gvim --remote-silent .git/index'
alias vs='vim .git/index'

# clear
alias c='clear'
alias cl='clear; l'
alias cls='clear; ls'

# processes
alias tu='top -o cpu'   # cpu
alias tm='top -o vsize' # memory

# git
alias ungit="find . -name '.git' -exec rm -rf {} \;"
alias gb='git branch'
alias gba='git branch -a'
alias gbd='git branch -d'
alias gc='git commit -v'
alias gca='git commit -v -a'
alias gls='git ls-files'
alias gcx='git clean -xdf'
alias gac='git update-index --assume-unchanged'
alias ganc='git update-index --no-assume-unchanged'
alias sac='git ls-files -v | grep "^[[:lower:]]"'
alias gh='bash -c "source ~/.githelpers && show_git_head"'
alias gs='bash -c "source ~/.githelpers && pretty_git_log"'

alias gco='git checkout'
alias gd='git diff'
alias gdm='git diff master'
alias gl='git pull'
alias glo='git pull origin master'
alias gp='git push'
alias gpo='git push origin master'
alias g='git status'
alias gk='git --no-pager log --pretty=oneline --all --decorate -30 --graph --abbrev-commit'
alias gkb='git --no-pager log --pretty=oneline --decorate -30 --graph --abbrev-commit'
alias gkn='git log --pretty=oneline --all --decorate --graph --abbrev-commit'
alias glg='git lg'

# finding grep
find_function() {
    find . -name "*$1*"
}
alias afind='ack -il'
alias ffind='find_function'

# containers
alias lc='sudo lxc-ls -f'
alias lcs='sudo lxc-start -d -n '
alias lcp='sudo lxc-stop -n '
alias lcd='sudo lxc-destroy -f -n '
alias lcd='sudo lxc-destroy -f -n '

# containers
alias drc='docker rm -f $(docker ps -a -q)'
alias dri='docker rmi -f $(docker images -q)'

# convenience
alias ka='killall -9 chrome &>/dev/null; pulseaudio -k &>/dev/null'
alias pd='poweroff'

# consul
alias consul-cli='consul-cli --ssl --ssl-ca-cert tls/consul-ca.pem --ssl-cert tls/cli.pem --ssl-key tls/cli-key.pem --consul https://consul.service.infra.devops.roche.com:8501'

# options
unsetopt correct_all
setopt nocheckjobs
setopt nohup

# use vv instead of esc for vim mode
bindkey -v
bindkey -M viins 'vv' vi-cmd-mode

# stuffed moved from bin
alias back='bash -c "feh --bg-scale ~/setup/images/background.png"'
